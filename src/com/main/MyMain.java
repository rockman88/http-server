package com.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class MyMain {
	public static void main(String[] args) throws IOException {
		ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

		HttpServer server = HttpServer.create(new InetSocketAddress("localhost", 8001), 0);
		server.createContext("/", new MyHandler());
		server.setExecutor(threadPoolExecutor);
		server.start();
	}
}

class MyHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange he) throws IOException {
		String root = ".";
		URI uri = he.getRequestURI();
		System.out.println("looking for: " + root + uri.getPath());
		String path = uri.getPath();
		File file = new File(root + path).getCanonicalFile();

		if (!file.isFile()) {
			// Object does not exist or is not a file: reject with 404 error.
			String response = "404 (Not Found)\n";
			he.sendResponseHeaders(404, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.getBytes());
			os.close();
		} else {
			// Object exists and is a file: accept with response code 200.
			String mime;
			if (path.substring(path.length() - 3).equals(".js"))
				mime = "application/javascript";
			else if (path.substring(path.length() - 3).equals("css"))
				mime = "text/css";
			else if (path.substring(path.length() - 3).equals("xml"))
				mime = "text/xml";
			else
				mime = "application/octet-stream";

			Headers h = he.getResponseHeaders();
			h.set("Content-Type", mime);
			he.sendResponseHeaders(200, 0);

			OutputStream os = he.getResponseBody();
			FileInputStream fs = new FileInputStream(file);
			final byte[] buffer = new byte[0x10000];
			int count = 0;
			while ((count = fs.read(buffer)) >= 0) {
				os.write(buffer, 0, count);
			}
			fs.close();
			os.close();
		}
	}
}
